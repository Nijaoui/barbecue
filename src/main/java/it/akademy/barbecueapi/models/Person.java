package it.akademy.barbecueapi.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import it.akademy.barbecueapi.models.Person;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Person extends Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private int id;
    private String lastName;
    private String firstName;
    private String job;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Food> foods;

    public Person(){}

    public Person(int id, String lastName, String firstName, String job) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.job = job;
        this.foods = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }
     
     
    public void eatFood(Person person, Food food) {
        System.out.println(person.getName() + "eat" + food.getName());
    }
}


