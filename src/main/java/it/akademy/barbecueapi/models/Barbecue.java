package it.akademy.barbecueapi.models;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Barbecue{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   
    private int id;
    private String name;
    private String address;
    private String city;
    private String country;


    @JsonManagedReference(value = "barbecue-persons")
    @OneToMany
    private List<Person> persons;

    @JsonManagedReference(value = "food-barbecue")
    @OneToMany
    private List<Food> foods;

    public Barbecue(){}

    public Barbecue(int id, String name, String address, String city, String country ){
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.country = country;
        this.persons = new ArrayList<>();
        this.foods = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAdress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }
    
    
}
