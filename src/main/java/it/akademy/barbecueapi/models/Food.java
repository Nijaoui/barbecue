package it.akademy.barbecueapi.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Food extends Barbecue{

    @Id
    private int id;

    private String name;
    

    @JsonBackReference
    @ManyToOne
    private Person person;

    public Food(){}

    public Food(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    public void afficheName(Food food){
        System.out.println(food.getName() + " existe à la liste");
    }
    
    public void searchFood(Food food){
        System.out.println(food.getAdress() + " existe à cette adresse");
    }

}
