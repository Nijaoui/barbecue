package it.akademy.barbecueapi.controllers;

import it.akademy.barbecueapi.dao.FoodDao;
import it.akademy.barbecueapi.dao.PersonDao;
import it.akademy.barbecueapi.models.Food;
import it.akademy.barbecueapi.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/persons")
public class PersonController {

    private final PersonDao personDao;
    private final FoodDao foodDao;

    @Autowired
    public PersonController(PersonDao personDao, FoodDao foodDao){
        this.personDao = personDao;
        this.foodDao = foodDao;
    }

    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons(){
        List<Person> persons = personDao.findAll();
        return new ResponseEntity<>(persons, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable int id){
        Person person = personDao.findById(id);

        if(person == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @GetMapping("/{id}/foods")
    public ResponseEntity<List<Food>> getFoodsByPerson(@PathVariable int id){
        Person person = personDao.findById(id);

        if(person == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Food> foods = foodDao.findAllByPerson(person);

        return new ResponseEntity<>(foods, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Person> createPerson(@RequestBody Person person){
        Person addedPerson = personDao.save(person);
        return new ResponseEntity<>(addedPerson, HttpStatus.CREATED);
    }

    @PutMapping("/{personId}/foods/{foodId}")
    public ResponseEntity<Person> addFoodInPerson(@PathVariable int personId, @PathVariable int foodId){
        Person person = personDao.findById(personId);

        if(person == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Food food = foodDao.findById(foodId);

        if(food == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        person.getFoods().add(food);
        food.setPerson(person);
        person.setId(personId);
        personDao.save(person);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }
}
