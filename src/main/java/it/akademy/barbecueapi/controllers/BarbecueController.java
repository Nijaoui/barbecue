package it.akademy.barbecueapi.controllers;

import it.akademy.barbecueapi.dao.FoodDao;
import it.akademy.barbecueapi.dao.PersonDao;

import it.akademy.barbecueapi.dao.BarbecueDao;

import it.akademy.barbecueapi.models.Barbecue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/barbecues")
public class BarbecueController {

    private final BarbecueDao barbecueDao;
    private final FoodDao foodDao;
    private final PersonDao personDao;

    @Autowired
    public BarbecueController(BarbecueDao barbecueDao, FoodDao foodDao, PersonDao personDao){
        this.barbecueDao = barbecueDao;
        this.foodDao = foodDao;
        this.personDao = personDao;
    }

    @GetMapping
    public ResponseEntity<List<Barbecue>> getAllBarbecues(){
        List<Barbecue> barbecues = barbecueDao.findAll();
        return new ResponseEntity<>(barbecues, HttpStatus.OK);
    }

    @GetMapping("/?name={name}")
    public ResponseEntity<List<Barbecue>> getAllBarbecueByName(@PathVariable String name){
        List<Barbecue> barbecue = barbecueDao.findAllByName(name);
        return new ResponseEntity<>(barbecue, HttpStatus.OK);
    }

    @GetMapping("/?id={id}")
    public ResponseEntity<Barbecue> getBarbecueById(@PathVariable int id){
        Barbecue barbecue = barbecueDao.findById(id);
        if(barbecue == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(barbecue, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Barbecue> addBarbecue(@RequestBody Barbecue barbecue){
        Barbecue addedBarbecue = barbecueDao.save(barbecue);
        return new ResponseEntity<>(addedBarbecue, HttpStatus.CREATED);
    }
}


