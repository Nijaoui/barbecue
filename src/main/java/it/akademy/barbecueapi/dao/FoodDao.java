package it.akademy.barbecueapi.dao;

import it.akademy.barbecueapi.models.Food;
import it.akademy.barbecueapi.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodDao extends JpaRepository<Food, Integer> {

    @Override
    List<Food> findAll();

    List<Food> findAllByPerson(Person person);

    Food findById(int id);

    Food save(Food food);

    void deleteById(int id);
}
