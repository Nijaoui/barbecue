package it.akademy.barbecueapi.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.akademy.barbecueapi.models.Barbecue;

@Repository
public interface BarbecueDao extends JpaRepository<Barbecue, Integer> {

    List<Barbecue> findAll();

    List<Barbecue> findAllByName(String name);

    Barbecue findById(int id);

    Barbecue save(Barbecue barbecue);

    void deleteById(int id);
}
